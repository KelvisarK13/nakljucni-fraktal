# NAKLJUČNI FRAKTAL #

### Opis ###

Program, ki nariše fraktal glede na vnešena števila.

### Vsebina repozitorija ###

* `NakljucniFraktal.java`: glavno okno

* `KompleksnaRavnina.java`: skrbi za izris na zaslon

* `KompleksnoStevilo`: pomožni atributi


### O repozitoriju ###

Repozitorij 'Nakljucni fraktal' vsebuje projekt pri predmetu Programiranje 2 (študijsko leto 2014/2015). Napisan je v programskem jeziku Java.