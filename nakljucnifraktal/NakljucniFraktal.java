/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nakljucnifraktal;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author Katja
 */

@SuppressWarnings("serial")
public class NakljucniFraktal extends JFrame implements ActionListener, ChangeListener {

    // polja:
    
   
    private KompleksnaRavnina kompleksnaRavnina;
    private JSlider sliderIteracij;
    private JTextField koeficientiField;
    
    public NakljucniFraktal(){
        super();
        
        this.setTitle("Nakljucni fraktal");
        this.setSize(new Dimension(800,800));
        
        setLayout(new GridBagLayout());
        GridBagConstraints polozaj = new GridBagConstraints();
			
        polozaj = new GridBagConstraints();
                
        polozaj.gridx = 0;
        polozaj.gridy = 0;
        polozaj.gridwidth = 1;
        
        JButton zoomGumb = new JButton("Zoom-out");
        zoomGumb.setActionCommand("zoom-out");
        zoomGumb.addActionListener(this);
        this.add(zoomGumb, polozaj);
        
        polozaj.gridx = 1;
        polozaj.gridy = 1;
        polozaj.weightx = 1;
        polozaj.fill = GridBagConstraints.BOTH;
        
        this.koeficientiField = new JTextField();
        this.koeficientiField.setToolTipText("<html><p width=\"300\">" + "Vpisite poljubna realna stevila in jih locite z vejicami, namesto decimalne vejice pa uporabite piko. Priporocljivo je, da je prvo stevilo, ki ga vnesete, manjse od 1."
                + "<br> Primer (Mandelbrotova mnozica - privzeto): 0, 1" + "</p></html>");
        this.add(this.koeficientiField, polozaj);
        
        polozaj.gridx = 0;
        polozaj.gridy = 1;
        polozaj.weightx = 0;
        
        JLabel koeficientiLabel = new JLabel("Nastavite koeficiente:  ");
        this.add(koeficientiLabel, polozaj);
             
        polozaj.gridx = 2;
        polozaj.gridy = 1;
        polozaj.weightx = 0;
        
        JButton nastaviGumb = new JButton("Nastavi");
        nastaviGumb.setActionCommand("nastavi");
        nastaviGumb.addActionListener(this);
        this.add(nastaviGumb, polozaj);
        
        polozaj.gridx = 0;
        polozaj.gridy = 2;
        
        JLabel iteracijeLabel = new JLabel("Stevilo iteracij:");
        this.add(iteracijeLabel, polozaj);
        
        polozaj.gridx = 0;
        polozaj.gridy = 3;
        polozaj.gridwidth = GridBagConstraints.REMAINDER;
        polozaj.fill = GridBagConstraints.BOTH;
        
        this.sliderIteracij = new JSlider(JSlider.HORIZONTAL, 0, 200, 0);
        this.add(sliderIteracij, polozaj);
        
        this.sliderIteracij.addChangeListener(this);
        this.sliderIteracij.setPaintTrack(true);
        this.sliderIteracij.setFont(new Font("Tahoma", Font.BOLD, 12));
        this.sliderIteracij.setMajorTickSpacing(10);
        this.sliderIteracij.setMinorTickSpacing(5);
        this.sliderIteracij.setPaintLabels(true);
        this.sliderIteracij.setPaintTicks(true);
        this.sliderIteracij.setPaintTrack(true);
        this.sliderIteracij.setAutoscrolls(true);
        
        polozaj.gridx = 0;
        polozaj.gridy = 4;
        polozaj.gridwidth = 3;
        polozaj.weightx = 1;
        polozaj.weighty = 1;
        polozaj.anchor = GridBagConstraints.CENTER;
        polozaj.fill = GridBagConstraints.BOTH;
        
        this.kompleksnaRavnina = new KompleksnaRavnina();
        this.kompleksnaRavnina.setBackground(Color.white);
        this.kompleksnaRavnina.setToolTipText("Za povecavo oznacite zeljeno obmocje.");
        this.add(this.kompleksnaRavnina, polozaj);
    }
    
    
    public void actionPerformed(ActionEvent ae) {
        if (ae.getActionCommand() == "zoom-out"){
            this.kompleksnaRavnina.setxMin(this.kompleksnaRavnina.getxMin() - 1);
            this.kompleksnaRavnina.setxMax(this.kompleksnaRavnina.getxMax() + 1);
            this.kompleksnaRavnina.setyMin(this.kompleksnaRavnina.getyMin() - 0.5);
            this.kompleksnaRavnina.setyMax(this.kompleksnaRavnina.getyMax() + 0.5);
            this.kompleksnaRavnina.narisi();
        }
       
        else if (ae.getActionCommand() == "nastavi"){
            String koeficienti = this.koeficientiField.getText();
            String[] koeficientiTabela = koeficienti.split(",");
            double[] doubleKoeficienti = new double[koeficientiTabela.length];
            for (int i = 0; i < koeficientiTabela.length; i += 1){
                try {
                    doubleKoeficienti[i] = Double.parseDouble(koeficientiTabela[i]);
                } catch (Exception izjema){
                    doubleKoeficienti[i] = 0.0;
                }
            }
            this.kompleksnaRavnina.setDoubleKoeficienti(doubleKoeficienti);
            this.kompleksnaRavnina.setxMin(-2);
            this.kompleksnaRavnina.setxMax(2);
            this.kompleksnaRavnina.setyMin(-2);
            this.kompleksnaRavnina.setyMax(2);
            this.kompleksnaRavnina.narisi();
        }
    }

    
    

    public void stateChanged(ChangeEvent ce) {
        this.kompleksnaRavnina.setIteracije(this.sliderIteracij.getValue());
        this.kompleksnaRavnina.narisi();
    }    
    
    
    public static void main(String[] args) {
        NakljucniFraktal nF = new NakljucniFraktal();
        nF.setVisible(true);
    }


    

   
    
}
