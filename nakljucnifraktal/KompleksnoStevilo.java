/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nakljucnifraktal;

/**
 *
 * @author Katja
 */
public class KompleksnoStevilo {
    
    // polja
    private double x;
    private double y;

    public KompleksnoStevilo(double x, double y) {
        this.x=x;
        this.y=y;
    }
    
    public KompleksnoStevilo sestej(KompleksnoStevilo other){
        double x0 = this.x + other.x;
        double y0 = this.y + other.y;
        return new KompleksnoStevilo(x0, y0);
    }
    
    public KompleksnoStevilo sestej(double other){
        double x0 = this.x + other;
        return new KompleksnoStevilo(x0, this.y);
    }
    
    public KompleksnoStevilo zmnozi(KompleksnoStevilo other){
        double x0 = this.x*other.x - this.y*other.y;
        double y0 = this.x*other.y + this.y*other.x;
        return new KompleksnoStevilo(x0, y0);
    }
    
    public KompleksnoStevilo zmnozi(double other){
        double x0 = this.x*other;
        double y0 = this.y*other;
        return new KompleksnoStevilo(x0, y0);
    }
    
    public KompleksnoStevilo polinom (double[] koeficienti){
        KompleksnoStevilo vrni = new KompleksnoStevilo(0, 0);
        for (int i = koeficienti.length-1; i > -1; i -= 1){
            vrni = vrni.sestej(koeficienti[i]).zmnozi(this);
        }
        return vrni;
    }
    
    public double absolutno(){
        return Math.pow(Math.pow(x, 2) + Math.pow(y, 2), 0.5);
        }
    
    public double argument(){
        return Math.atan2(y, x);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    } 
   
    
    
    
}
