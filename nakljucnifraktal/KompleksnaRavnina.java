/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nakljucnifraktal;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 *
 * @author Katja
 */
public class KompleksnaRavnina extends JPanel implements MouseListener{
    
    private double xMin = -2.5;
    private double xMax = 2.5;
    private double yMin = -2.5;
    private double yMax = 2.5;
    
    private int x0;
    private int y0;
    
    private int iteracije;
    
    private double[] doubleKoeficienti;
    
    private BufferedImage slika;
    
    
    
    public KompleksnaRavnina(){
        super();
        this.addMouseListener(this);
        this.iteracije = 0;
        this.doubleKoeficienti = new double[] {0.0, 1.0};
        
      this.slika = new BufferedImage(800, 800, BufferedImage.TYPE_INT_RGB);
    }
    
    public KompleksnoStevilo vKompleksno(int i, int j){
        double x0 = j*(xMax-xMin)/this.getWidth() + xMin;
        double y0 = -i*(yMax-yMin)/this.getHeight() + yMax;
        return new KompleksnoStevilo(x0, y0);
    }

    public void setxMin(double xMin) {
        this.xMin = xMin;
    }

    public void setxMax(double xMax) {
        this.xMax = xMax;
    }

    public void setyMin(double yMin) {
        this.yMin = yMin;
    }

    public void setyMax(double yMax) {
        this.yMax = yMax;
    }

    public void setIteracije(int iteracije) {
        this.iteracije = iteracije;
    }

    public void setDoubleKoeficienti(double[] doubleKoeficienti) {
        this.doubleKoeficienti = doubleKoeficienti;
    }
    
    public double getxMin() {
        return xMin;
    }

    public double getxMax() {
        return xMax;
    }

    public double getyMin() {
        return yMin;
    }

    public double getyMax() {
        return yMax;
    }
    
    
    
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(slika.getWidth(), slika.getHeight());
    }
    
    public void izracunajSliko(){
        this.slika = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB);
        for(int i=0; i<this.slika.getHeight(); i+=1){
            for(int j=0; j<this.slika.getWidth(); j+=1){
                KompleksnoStevilo c = this.vKompleksno(i, j);
                KompleksnoStevilo c0 = c;
                int stIteracij = this.iteracije;
                while(true){
                    if (stIteracij == 0){
                        break;
                    }
                    else if (c.absolutno() >= 10){
                        break;
                    }
                    c = c.polinom(doubleKoeficienti).sestej(c0);
                    stIteracij -= 1;
                }
                int rdeca = (int) Math.max((c.absolutno()-10)*(-25.5), 0); //linearno raztegujemo, norma 0 je bela, norma vec od 10 pa temno modra (arctan)
                int zelena = rdeca; //isto kot pri rdeci
                int modra = (int) (255- 200 * (2/Math.PI) * Math.atan(c.absolutno()));
                Color barva = new Color(rdeca, zelena, modra);
                int rgb = barva.getRGB();
                this.slika.setRGB(j, i, rgb);
            }
        }  
    }
    
    
    public void narisi() {
        Runnable slikar = new Runnable() {
            public void run() {
                izracunajSliko();
                repaint();
            }
        };
        new Thread(slikar).start();			
    }
    
    
    
    
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g); 
        g.drawImage(this.slika, 0, 0, this.slika.getWidth(), this.slika.getHeight(), null);
    }


    public void mouseClicked(MouseEvent me) {
    }

    public void mousePressed(MouseEvent me) {
        x0 = me.getX();
        y0 = me.getY();
    }


    public void mouseReleased(MouseEvent me) {
        if (! (Math.abs(x0-me.getX()) < 10 || Math.abs(y0-me.getY()) < 10)){
            KompleksnoStevilo zgLeviKlik = vKompleksno(y0, x0);
            KompleksnoStevilo spDesniKlik = vKompleksno(me.getY(), me.getX());
            xMin = zgLeviKlik.getX();
            xMax = spDesniKlik.getX();
            yMin = spDesniKlik.getY();
            yMax = zgLeviKlik.getY();
            this.narisi();
        } 
    }


    public void mouseEntered(MouseEvent me) {
    }


    public void mouseExited(MouseEvent me) {
    }
    
    
    
    
}
